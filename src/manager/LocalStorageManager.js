
class LocalStorageManager {

    constructor() {
        this.storage = null, this.sheet = {};
        if (typeof(Storage) !== "undefined") {
            this.storage = window.localStorage;
            this.sheet = {
                noOfRows: 10,
                noOfColumns: 10,
                gridValue: [
                    ["", "", "", "", "", "", "", "", "", ""],
                    ["", "", "", "", "", "", "", "", "", ""],
                    ["", "", "", "", "", "", "", "", "", ""],
                    ["", "", "", "", "", "", "", "", "", ""],
                    ["", "", "", "", "", "", "", "", "", ""],
                    ["", "", "", "", "", "", "", "", "", ""],
                    ["", "", "", "", "", "", "", "", "", ""],
                    ["", "", "", "", "", "", "", "", "", ""],
                    ["", "", "", "", "", "", "", "", "", ""],
                    ["", "", "", "", "", "", "", "", "", ""],
                ],
                // [colId, colWidth]
                colWidth: [],
                // [rowId, rowHeight]
                rowHeight: [],
            };
            this.setDefaultValues();
        } else {
            console.log("Local Storage is not supported by browser. The data will not be persisted.");
        }
    };

    saveItem = (key, value) => {
        if(typeof(Storage) == "undefined") {
            return;
        }
        if(key == "noOfRows" || key == "noOfColumns" || key == "gridValue" || key == "colWidth" || key == "rowHeight") {
            var newSheet = this.getItem("sheet");
            newSheet[key] = value;
            this.storage.setItem("sheet", JSON.stringify(newSheet));
        } else {
            this.storage.setItem(key, JSON.stringify(value));
        }
    };

    getLocalData = () => {
        if(typeof(Storage) == "undefined") {
            return null;
        }
        return JSON.parse(this.storage.getItem("sheet"));
    };

    getItem = (key) => {
        if(typeof(Storage) == "undefined") {
            return null;
        }
        return JSON.parse(this.storage.getItem(key));
    };

    setDefaultValues = () => {
        if(this.getItem("sheet") == null) {
            this.storage.setItem("sheet", JSON.stringify(this.sheet));
        }
    };

};

export default new LocalStorageManager();
