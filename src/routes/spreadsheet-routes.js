var express = require('express');
const router = express.Router();

var React = require('react');
var ReactDOMServer = require('react-dom/server');

router.use(function timeLog(req, res, next) {
    console.log('Time: ', Date.now());
    next();
});

router.get('/', function(req, res) {
    res.render('../../views/index', {reactOutput: ""});
});

module.exports = router;
