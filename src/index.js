var express = require('express');
var bodyParser = require('body-parser');
var multer = require('multer');
var morgan = require('morgan');
var path = require('path');
var http = require('http');

const port = (process.env.PORT || 5000);
const app = express();
const upload = multer();

app.use(express.static(__dirname + '/../public'));
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

var spreadSheetRoutes = require('./routes/spreadsheet-routes');
app.use('/sheet', spreadSheetRoutes);
app.use('/', spreadSheetRoutes);

app.get('/test', (req, res) => {
    res.send('Excel Sheets!');
});

console.log('NODE_ENV: ' + process.env.NODE_ENV);

const httpServer = http.createServer(app);
httpServer.listen(port, function () {
    console.log('Express listening on port ' + port);
});
