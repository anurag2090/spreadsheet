import React, {Component} from 'react';
import Header from './../common/Header';
import SpreadSheet from './SpreadSheet';
import LocalStorageManager from './../../manager/LocalStorageManager';

class SSPage extends Component {

    static get defaultProps() {
        return {
            noOfRows: 10,
            noOfColumns: 10,
        }
    };

    constructor(props, context) {
        super(props, context);
        this.state = {
            noOfRows: LocalStorageManager.getLocalData().noOfRows,
            noOfColumns: LocalStorageManager.getLocalData().noOfColumns,
        };
    };

    render = () => {
        return (
            <div>
                <Header />
                <SpreadSheet noOfRows={this.state.noOfRows} noOfColumns={this.state.noOfColumns} />
            </div>
        );
    };
}

export default SSPage;
