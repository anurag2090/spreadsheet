import React, {Component} from 'react';
import LocalStorageManager from './../../manager/LocalStorageManager';

var selectedMatrix = [], copiedMatrix = [], pasteDestMatrix = [];

class Cell extends Component {

    static get defaultProps() {
        return {
            id: "",
            rowNumber: "",
            colNumber: "",
            text: "",
            focus: false,
            editable: false,
        }
    };

    constructor(props, context) {
        super(props, context);
        this.state = {
            id: props.id,
            rowNumber: props.rowNumber,
            colNumber: props.colNumber,
        };
    };

    onBlur = (event) => {
        this.setState({
            text: event.target.value,
            focus: false,
            editable: false,
        });
        if(typeof(gridValue[this.state.rowNumber]) == "undefined" || gridValue[this.state.rowNumber] == null) {
            gridValue[this.state.rowNumber] = [];
        }
        gridValue[this.state.rowNumber][this.state.colNumber] = event.target.value;
        LocalStorageManager.saveItem("gridValue", gridValue);
        event.target.parentElement.classList.remove("selected");
    };

    onClick = (event) => {
        this.setState({
            focus: true,
            editable: true,
        });
        var selected = document.getElementsByClassName("selected");
        while (selected.length)
            selected[0].className = selected[0].className.replace(/\bselected\b/g, "");
        selectedMatrix = [];

        if(selectedMatrix.length==0) {
            event.target.parentElement.className += " selected";
            selectedMatrix[0] = [];
            selectedMatrix[0][0] = event.target;
        }
    };

    onFocus = (event) => {
        var selected = document.getElementsByClassName("selected");
        while (selected.length)
            selected[0].className = selected[0].className.replace(/\bselected\b/g, "");
        selectedMatrix = [];

        if(selectedMatrix.length==0) {
            event.target.parentElement.className += " selected";
            selectedMatrix[0] = [];
            selectedMatrix[0][0] = event.target;
        }
    };

    onKeyDown = (event) => {
        var el = event.target;
        if(event.ctrlKey) {
            // down arrow
            if(event.keyCode == 40) {
                if(selectedMatrix.length==0) {
                    el.parentElement.className += " selected";
                    selectedMatrix[0] = [];
                    selectedMatrix[0][0] = el;
                }
                // only last row with all columns, we need here, j is column number here
                var prevLength = selectedMatrix.length;
                for(var j=0; j<selectedMatrix[prevLength-1].length; j++) {
                    var tempEl = selectedMatrix[prevLength-1][j];
                    var tempElId = tempEl.id;
                    var temp = tempElId.split("-");
                    var tRow = temp[1];
                    var tCol = temp[2];

                    tRow++;
                    if(document.getElementById("rc-"+tRow+"-"+tCol) != null) {
                        document.getElementById("rc-"+tRow+"-"+tCol).parentElement.className += " selected";
                        if(typeof(selectedMatrix[prevLength]) == "undefined" || selectedMatrix[prevLength] == null) {
                            selectedMatrix[prevLength] = [];
                        }
                        selectedMatrix[prevLength][j] = document.getElementById("rc-"+tRow+"-"+tCol);
                    } else {
                        tRow--;
                    }
                }
            }

            // up arrow
            if(event.keyCode == 38) {
                if(selectedMatrix.length==0) {
                    el.parentElement.className += " selected";
                    selectedMatrix[0] = [];
                    selectedMatrix[0][0] = el;
                }

                // shift one row down to create space for new top row
                for(var j=selectedMatrix.length-1; j>=0; j--) {
                    selectedMatrix[j+1] = selectedMatrix[j];
                }
                // only first row with all columns, we need here, j is column number here
                for(var j=0; j<selectedMatrix[0].length; j++) {
                    var tempEl = selectedMatrix[0][j];
                    var tempElId = tempEl.id;
                    var temp = tempElId.split("-");
                    var tRow = temp[1];
                    var tCol = temp[2];

                    tRow--;
                    if(document.getElementById("rc-"+tRow+"-"+tCol) != null) {
                        document.getElementById("rc-"+tRow+"-"+tCol).parentElement.className += " selected";
                        if(typeof(selectedMatrix[0]) == "undefined" || selectedMatrix[0] == null) {
                            selectedMatrix[0] = [];
                        }
                        selectedMatrix[0][j] = document.getElementById("rc-"+tRow+"-"+tCol);
                    } else {
                        tRow++;
                    }
                }
            }

            // left arrow
            if(event.keyCode == 37) {
                if(selectedMatrix.length==0) {
                    el.parentElement.className += " selected";
                    selectedMatrix[0] = [];
                    selectedMatrix[0][0] = el;
                }

                // shift one col right to create space for new left col
                for(var i=0; i<selectedMatrix.length; i++) {
                    for(var j=selectedMatrix[i].length-1; j>=0; j--) {
                        selectedMatrix[i][j+1] = selectedMatrix[i][j];
                    }
                }
                // only first column, we need here, j is row number here
                for(var j=0; j<selectedMatrix.length; j++) {
                    var tempEl = selectedMatrix[j][0];
                    var tempElId = tempEl.id;
                    var temp = tempElId.split("-");
                    var tRow = temp[1];
                    var tCol = temp[2];

                    tCol--;
                    if(document.getElementById("rc-"+tRow+"-"+tCol) != null) {
                        document.getElementById("rc-"+tRow+"-"+tCol).parentElement.className += " selected";
                        if(typeof(selectedMatrix[j]) == "undefined" || selectedMatrix[j] == null) {
                            selectedMatrix[j] = [];
                        }
                        selectedMatrix[j][0] = document.getElementById("rc-"+tRow+"-"+tCol);
                    } else {
                        tCol++;
                    }
                }
            }

            // right arrow
            if(event.keyCode == 39) {
                if(selectedMatrix.length==0) {
                    el.parentElement.className += " selected";
                    selectedMatrix[0] = [];
                    selectedMatrix[0][0] = el;
                }
                // only last column, we need here, j is row number here
                var prevLength = selectedMatrix.length;
                for(var j=0; j<prevLength; j++) {
                    var tempEl = selectedMatrix[j][selectedMatrix[j].length-1];
                    var tempElId = tempEl.id;
                    var temp = tempElId.split("-");
                    var tRow = temp[1];
                    var tCol = temp[2];

                    tCol++;
                    if(document.getElementById("rc-"+tRow+"-"+tCol) != null) {
                        document.getElementById("rc-"+tRow+"-"+tCol).parentElement.className += " selected";
                        if(typeof(selectedMatrix[j]) == "undefined" || selectedMatrix[j] == null) {
                            selectedMatrix[j] = [];
                        }
                        selectedMatrix[j][selectedMatrix[j].length] = document.getElementById("rc-"+tRow+"-"+tCol);
                    } else {
                        tCol--;
                    }
                }
            }

            // c key
            if(event.keyCode == 67) {
                if(selectedMatrix.length == 0) {
                    alert("nothing selected to copy");
                    return;
                }
                copiedMatrix = selectedMatrix;
                alert("data copied");
            }

            // v key
            if(event.keyCode == 86) {
                if(copiedMatrix.length == 0) {
                    alert("nothing to copy");
                    return;
                }
                pasteDestMatrix = selectedMatrix;
                if(copiedMatrix.length != pasteDestMatrix.length) {
                    alert("please select equal size of grid for copy and paste");
                    return;
                }

                for(var i=0; i<copiedMatrix.length; i++) {
                    for(var j=0; j<copiedMatrix[i].length; j++) {
                        pasteDestMatrix[i][j].value = copiedMatrix[i][j].value;
                        pasteDestMatrix[i][j].focus();
                        pasteDestMatrix[i][j].blur();
                    }
                }
                copiedMatrix = [];
                pasteDestMatrix = [];
            }

            // b bold key
            if(event.keyCode == 66) {
                for(var i=0; i<selectedMatrix.length; i++) {
                    for(var j=0; j<selectedMatrix[i].length; j++) {
                        if(!selectedMatrix[i][j].classList.contains("bold")) {
                            selectedMatrix[i][j].className += " bold";
                        } else {
                            selectedMatrix[i][j].classList.remove("bold");
                        }
                    }
                }
            }

            // i italic key
            if(event.keyCode == 73) {
                for(var i=0; i<selectedMatrix.length; i++) {
                    for(var j=0; j<selectedMatrix[i].length; j++) {
                        if(!selectedMatrix[i][j].classList.contains("italic")) {
                            selectedMatrix[i][j].className += " italic";
                        } else {
                            selectedMatrix[i][j].classList.remove("italic");
                        }
                    }
                }
            }

            // m underline key
            if(event.keyCode == 77) {
                for(var i=0; i<selectedMatrix.length; i++) {
                    for(var j=0; j<selectedMatrix[i].length; j++) {
                        if(!selectedMatrix[i][j].classList.contains("underline")) {
                            selectedMatrix[i][j].className += " underline";
                        } else {
                            selectedMatrix[i][j].classList.remove("underline");
                        }
                    }
                }
            }
        }
        else {
            var temp = el.id, row, col;
            temp = temp.split("-");
            row = temp[1];
            col = temp[2];
            // down key
            if(event.keyCode == 40) {
                row++;
                if(document.getElementById("rc-"+row+"-"+col) != null) {
                    document.getElementById("rc-"+row+"-"+col).focus();
                } else {
                    row--;
                }
            }
            // left key
            if(event.keyCode == 37) {
                col--;
                if(document.getElementById("rc-"+row+"-"+col) != null) {
                    document.getElementById("rc-"+row+"-"+col).focus();
                } else {
                    col++;
                }
            }
            // up key
            if(event.keyCode == 38) {
                row--;
                if(document.getElementById("rc-"+row+"-"+col) != null) {
                    document.getElementById("rc-"+row+"-"+col).focus();
                } else {
                    row++;
                }
            }
            // right key
            if(event.keyCode == 39) {
                col++;
                if(document.getElementById("rc-"+row+"-"+col) != null) {
                    document.getElementById("rc-"+row+"-"+col).focus();
                } else {
                    col--;
                }
            }
        }
    };

    render() {
        return (
		    <div className="cell">
			    <input id={this.props.id} value={this.state.value} onKeyDown={this.onKeyDown.bind(this)} onFocus={this.onFocus.bind(this)} onBlur={this.onBlur.bind(this)} onClick={this.onClick.bind(this)} type="text" />
		    </div>
        );
    }
}

export default Cell;
