import React, {Component} from 'react';
import Row from './Row';
import Column from './Column';
import Cell from './Cell';
import ContextMenu from './../context/ContextMenu';
import LocalStorageManager from './../../manager/LocalStorageManager';

class SpreadSheet extends Component {

    static get defaultProps() {
        return {
            id: "",
            noOfRows: 10,
            noOfColumns: 10,
        }
    };

    constructor(props, context) {
        super(props, context);
        this.state = {
            id: "",
            noOfRows: props.noOfRows,
            noOfColumns: props.noOfColumns,
            contextMenu: false,
            contextMenuX: 0,
            contextMenuY: 0,
            contextMenuType: "column",
            cmtId: null,
        };
    };

    drawCells = () => {
        var grid = [], i, j, idValue, row;
        for(i=0; i<this.state.noOfRows; i++) {
            row = this.drawRow(i);
            grid.push(<div key={"row-"+i} className="row">{row}</div>);
        }
        return grid;
    };

    drawRow = (rowNo) => {
        var row = [], j, idValue;
        for(j=0; j<this.state.noOfColumns; j++) {
            idValue = "rc-"+rowNo+"-"+j;
            row.push(<Cell key={idValue} id={idValue} rowNumber={rowNo} colNumber={j}/>);
            if(typeof(gridArr[rowNo]) == "undefined") {
                gridArr[rowNo] = [];
            }
            gridArr[rowNo][j] = idValue;
            if(typeof(gridRow[rowNo]) == "undefined") {
                gridRow[rowNo] = [];
            }
            gridRow[rowNo].push(idValue);
            if(typeof(gridCol[j]) == "undefined") {
                gridCol[j] = [];
            }
            gridCol[j].push(idValue);
        }
        return row;
    };

    fillDataIntoGridFromLS = () => {
        var localData = LocalStorageManager.getLocalData();
        gridValue = localData.gridValue;
        if(localData != null) {
            for(var row=0; row<localData.noOfRows; row++) {
                for (var col=0; col<localData.noOfColumns; col++) {
                    if(localData.gridValue[row] == null || localData.gridValue[row][col] == undefined || localData.gridValue[row][col] == "undefined")
                        continue;
                    document.getElementById("rc-"+row+"-"+col).value = localData.gridValue[row][col];
                }
            }
        }
    };

    componentDidMount = () => {
        this.fillDataIntoGridFromLS();
        this.bindContextMenuEventOnPage();
        this.subscribeToEvents();
    };

    bindContextMenuEventOnPage = () => {
        var self = this;
        window.oncontextmenu = function (event) {
            var temp = event.target.id, type;
            temp = temp.split("-");
            if(temp[0] == "col") {
                type = "column";
                self.setState({
                    contextMenu: true,
                    contextMenuX: event.pageX,
                    contextMenuY: event.pageY,
                    contextMenuType: type,
                    cmtId: event.target.id,
                });
                return false;     // cancel default menu
            } else if(temp[0] == "row") {
                type = "row";
                self.setState({
                    contextMenu: true,
                    contextMenuX: event.pageX,
                    contextMenuY: event.pageY,
                    contextMenuType: type,
                    cmtId: event.target.id,
                });
                return false;     // cancel default menu
            }
        }
        window.onclick = function(event) {
            if(event.target != document.getElementsByClassName("contextMenu")) {
                self.setState({
                    contextMenu: false,
                    contextMenuX: 0,
                    contextMenuY: 0,
                });
            }
        }
    };

    subscribeToEvents = () => {
        document.addEventListener("add_row", this.addRowHandler, false);
        document.addEventListener("remove_row", this.removeRowHandler, false);
        document.addEventListener("add_column", this.addColumnHandler, false);
        document.addEventListener("remove_column", this.removeColumnHandler, false);
    };

    componentWillUnmount = () => {
        document.removeEventListener("add_row", this.addRowHandler);
        document.removeEventListener("remove_row", this.removeRowHandler);
        document.removeEventListener("add_column", this.addColumnHandler);
        document.removeEventListener("remove_column", this.removeColumnHandler);
    };

    // after
    addRowHandler = (event) => {
        var targetRowId = event.detail.targetElement;
        var temp = targetRowId, targetRow;
        temp = temp.split("-");
        targetRow = parseInt(temp[1]);

        var gridValue = LocalStorageManager.getLocalData().gridValue;
        if(gridValue != null) {
            for(var i=gridValue.length-1; i>=targetRow; i--) {
                for(var j=0; j<gridValue[i].length; j++) {
                    if(typeof(gridValue[i+1]) == "undefined") {
                        gridValue[i+1] = [];
                    }
                    gridValue[i+1][j] = gridValue[i][j];
                }
            }

            for(var i=0; i<this.state.noOfColumns; i++) {
                if(typeof(gridValue[targetRow]) == "undefined") {
                    gridValue[targetRow] = [];
                }
                gridValue[targetRow][i] = "";
            }

            var rowCount = this.state.noOfRows;
            rowCount++;
            LocalStorageManager.saveItem("gridValue", gridValue);
            LocalStorageManager.saveItem("noOfRows", rowCount);
            this.setState({
                noOfRows: rowCount,
            });
        }
    };

    // after
    addColumnHandler = (event) => {
        var targetColId = event.detail.targetElement;
        var temp = targetColId, targetCol;
        temp = temp.split("-");
        targetCol = parseInt(temp[1]);

        var gridValue = LocalStorageManager.getLocalData().gridValue;
        if(gridValue != null) {
            for(var i=0; i<gridValue.length; i++) {
                var prevLength = gridValue[i].length;
                for(var j=prevLength-1; j>=targetCol; j--) {
                    if(typeof(gridValue[i]) == "undefined") {
                        gridValue[i] = [];
                    }
                    gridValue[i][j+1] = gridValue[i][j];
                }
            }

            for(var i=0; i<this.state.noOfRows; i++) {
                if(typeof(gridValue[i]) == "undefined") {
                    gridValue[i] = [];
                }
                gridValue[i][targetCol] = "";
            }

            var colCount = this.state.noOfColumns;
            colCount++;
            LocalStorageManager.saveItem("gridValue", gridValue);
            LocalStorageManager.saveItem("noOfColumns", colCount);
            this.setState({
                noOfColumns: colCount,
            });
        }
    };

    removeColumnHandler = (event) => {
        var targetColId = event.detail.targetElement;
        var temp = targetColId, targetCol;
        temp = temp.split("-");
        targetCol = parseInt(temp[1]);

        var gridValue = LocalStorageManager.getLocalData().gridValue;
        if(gridValue != null) {
            for(var i=0; i<gridValue.length; i++) {
                var prevLength = gridValue[i].length;
                for(var j=0; j<prevLength; j++) {
                    if(j > 0 && j > targetCol) {
                        gridValue[i][j-1] = gridValue[i][j];
                    }
                }
            }

            // delete column
            var gridValue = gridValue.map(function(val){
                return val.slice(0, -1);
            });

            var colCount = this.state.noOfColumns;
            colCount--;
            if(colCount < 0) {
                colCount = 0;
            }
            LocalStorageManager.saveItem("gridValue", gridValue);
            LocalStorageManager.saveItem("noOfColumns", colCount);
            this.setState({
                noOfColumns: colCount,
            });
        }
    };

    removeRowHandler = (event) => {
        var targetRowId = event.detail.targetElement;
        var temp = targetRowId, targetRow;
        temp = temp.split("-");
        targetRow = parseInt(temp[1]);

        var gridValue = LocalStorageManager.getLocalData().gridValue;
        if(gridValue != null) {
            for(var i=0; i<gridValue.length; i++) {
                var prevLength = gridValue[i].length;
                for(var j=0; j<prevLength; j++) {
                    if(i > 0 && i >= targetRow) {
                        gridValue[i-1][j] = gridValue[i][j];
                    }
                }
            }

            // delete row
            gridValue = gridValue.slice(0); // make copy
            gridValue.splice(gridValue.length - 1, 1);

            var rowCount = this.state.noOfRows;
            rowCount--;
            if(rowCount < 0) {
                rowCount = 0;
            }
            LocalStorageManager.saveItem("gridValue", gridValue);
            LocalStorageManager.saveItem("noOfRows", rowCount);
            this.setState({
                noOfRows: rowCount,
            });
        }
    };

    componentDidUpdate = () => {
        this.fillDataIntoGridFromLS();
    };

    render() {
        var cells = this.drawCells();

        return (
            <div className="spreadsheet">
                <Column count={this.state.noOfColumns} />
                <Row count={this.state.noOfRows} />
                <div className="cellGrid">
                    {cells}
                </div>

                <ContextMenu show={this.state.contextMenu} x={this.state.contextMenuX} y={this.state.contextMenuY} targetElement={{id: this.state.cmtId, type: this.state.contextMenuType}} />
            </div>
        );
    }
}

export default SpreadSheet;
