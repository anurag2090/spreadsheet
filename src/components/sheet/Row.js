import React, {Component} from 'react';

class Row extends Component {

    static get defaultProps() {
        return {
            id: "",
            count: 100,
        }
    };

    constructor(props, context) {
        super(props, context);
        this.state = {
            id: props.id,
            count: props.count,
        };
    };

    createRowHeader = () => {
        var row = [], i;
        for(i=0; i<=this.props.count; i++) {
            row.push(<div key={"row-"+i} id={"row-"+i} className="resizable-v">{i}</div>);
        }

        return row;
    };

    render() {
        var row = this.createRowHeader();

        return (
            <div className="rowHeader">
                {row}
            </div>
        );
    }
}

export default Row;
