import React, {Component} from 'react';

class Column extends Component {

    static get defaultProps() {
        return {
            id: "",
            count: 100,
        }
    };

    constructor(props, context) {
        super(props, context);
        this.state = {
            id: props.id,
            count: props.count,
        };
    };

    createColumnHeader = () => {
        var col = [], i;
        for (i = 0; i < this.props.count; i++) {
            col.push(<div key={"col-"+i} id={"col-"+i} className="resizable-h">{i}</div>);
        }

        return col;
    };

    render() {
        var col = this.createColumnHeader();

        return (
            <div className="colHeader">
                {col}
            </div>
        );
    }
}

export default Column;
