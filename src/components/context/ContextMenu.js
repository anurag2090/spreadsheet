import React, {Component} from 'react';

class ContextMenu extends Component {

    static get defaultProps() {
        return {
            targetElement: {
                id: "",
                type: "",
            },
            elementType: {
                column: {name: "column", menuItems: [
                    {
                        text: "Add a column before..",
                        action: "add_column",
                    },
                    {
                        text: "Remove this column",
                        action: "remove_column",
                    },
                ]},
                row: {name: "row", menuItems: [
                    {
                        text: "Add a row after..",
                        action: "add_row",
                    },
                    {
                        text: "Remove this row",
                        action: "remove_row",
                    },
                ]},
            }
        }
    };

    constructor(props, context) {
        super(props, context);
        this.state = {
            targetElement: props.targetElement,
            show: props.show,
            x: props.x,
            y: props.y,
        };
    };

    generateContextMenu = () => {
        var items = [], i = 0;
        for(var item of this.props.elementType[this.state.targetElement.type].menuItems) {
            if(item.action == "add_row") {
                items.push(<div key={"mitem-"+i} onClick={this.addRow.bind(this)} className="menuItem">{item.text}</div>);
            } else if(item.action == "add_column") {
                items.push(<div key={"mitem-"+i} onClick={this.addColumn.bind(this)} className="menuItem">{item.text}</div>);
            } else if(item.action == "remove_row") {
                items.push(<div key={"mitem-"+i} onClick={this.removeRow.bind(this)} className="menuItem">{item.text}</div>);
            } else if(item.action == "remove_column") {
                items.push(<div key={"mitem-"+i} onClick={this.removeColumn.bind(this)} className="menuItem">{item.text}</div>);
            }
            i++;
        }
        return items;
    };

    createAndDispatchEvent = (eventName, targetElement) => {
        var myEvent = new CustomEvent(eventName, {
    		detail: {
    			message: eventName,
    			targetElement: targetElement,
    		},
    		bubbles: true,
    		cancelable: true
    	});
        document.dispatchEvent(myEvent);
    };

    addRow = () => {
        this.createAndDispatchEvent("add_row", this.state.targetElement.id);
        this.setState({
            show: false,
        });
    };

    addColumn = () => {
        this.createAndDispatchEvent("add_column", this.state.targetElement.id);
        this.setState({
            show: false,
        });
    };

    removeRow = () => {
        this.createAndDispatchEvent("remove_row", this.state.targetElement.id);
        this.setState({
            show: false,
        });
    };

    removeColumn = () => {
        this.createAndDispatchEvent("remove_column", this.state.targetElement.id);
        this.setState({
            show: false,
        });
    };

    componentWillReceiveProps = (nextProps) => {
        if(this.props != nextProps) {
            this.setState({
                targetElement: nextProps.targetElement,
                show: nextProps.show,
                x: nextProps.x,
                y: nextProps.y,
            });
        }
    };

    render() {
        var menuItems = this.generateContextMenu();
        var className;
        if(this.state.show) {
            className = "contextMenu show";
        } else {
            className = "contextMenu hide";
        }

        return (
            <div className={className} style={{top: this.state.y+"px", left: this.state.x+"px"}}>
                {menuItems}
            </div>
        );
    }
}

export default ContextMenu;
