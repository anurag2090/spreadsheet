import React, {Component} from 'react';

class Header extends Component {

    render() {
        return (
            <header>
                <div className="heading">
                    SpreadSheet
                </div>
            </header>
        );
    };
}

export default Header;
