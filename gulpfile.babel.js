import gulp from 'gulp';
import shell from 'gulp-shell';
import rimraf from 'rimraf';
import run from 'run-sequence';
import watch from 'gulp-watch';
import server from 'gulp-live-server';

const paths = {
    js: ['./src/**/*.js', './src/**/*.json'],
    destination: './app'
}

gulp.task('default', cb => {
    run('server', 'build', 'watch', cb);
});

gulp.task('clean', cb => {
    run('clean', cb);
});

gulp.task('build', cb => {
    run('clean', 'babel', 'client-babel-development', 'restart', cb);
});

// run this on prod server
gulp.task('build_for_production', cb => {
    process.env.NODE_ENV = 'production';
    run('server', 'clean', 'babel', 'client-babel-production', cb);
});

gulp.task('clean', cb => {
    rimraf(paths.destination, cb);
});

gulp.task('babel', shell.task([
    'babel src --out-dir app'
]));

gulp.task('client-babel-production', shell.task([
    'browserify -g [envify --NODE_ENV "production"] -t [ babelify --presets [ react ] ] public/scripts/sspage-react.js -o public/scripts/sspage-react-bundle.js',
]));

gulp.task('client-babel-development', shell.task([
    'browserify -g [envify --NODE_ENV "development"] -t [ babelify --presets [ react ] ] public/scripts/sspage-react.js -o public/scripts/sspage-react-bundle.js',
]));

let express;

gulp.task('server', () => {
    express = server.new(paths.destination);
});

gulp.task('restart', () => {
    express.start.bind(express)();
});

gulp.task('watch', () => {
    return watch(paths.js, () => {
        gulp.start('build');
    });
});
