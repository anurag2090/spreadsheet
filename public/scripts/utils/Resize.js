'use strict';

var Resize = new function() {

    var resizableComponentsHorizontal = document.getElementsByClassName('resizable-h');
    var resizableComponentsVertical = document.getElementsByClassName('resizable-v');
    var startX, startY, startWidth, startHeight;

    this.init = function() {
        for(var item of resizableComponentsHorizontal) {
            var resizer = document.createElement('div');
            resizer.className = 'resizer-h';
            item.appendChild(resizer);
            item.addEventListener('mousedown', initDragHorizontal, false);
        };

        for(var item of resizableComponentsVertical) {
            var resizer = document.createElement('div');
            resizer.className = 'resizer-v';
            item.appendChild(resizer);
            item.addEventListener('mousedown', initDragVertical, false);
        };
    }

    var initDragHorizontal = function(e) {
        var el = e.target.parentElement;
        startX = e.clientX;
        if(el.classList.contains("resizable-h"))  {
            startWidth = parseInt(document.defaultView.getComputedStyle(e.target.parentElement).width, 10);
            document.documentElement.addEventListener('mousemove', doDragHorizontal, false);
            document.documentElement.addEventListener('mouseup', stopDrag, false);
            document.documentElement.addEventListener('mouseout', stopDrag, false);
        }
    }

    var initDragVertical = function(e) {
        var el = e.target.parentElement;
        startY = e.clientY;
        if(el.classList.contains("resizable-v"))  {
            startHeight = parseInt(document.defaultView.getComputedStyle(e.target.parentElement).height, 10);
            document.documentElement.addEventListener('mousemove', doDragVertical, false);
            document.documentElement.addEventListener('mouseup', stopDrag, false);
            document.documentElement.addEventListener('mouseout', stopDrag, false);
        }
    }

    var doDragHorizontal = function(e) {
        if(e.target.parentElement==null)
            return;

        e.target.parentElement.style.width = (startWidth + e.clientX - startX) + 'px';
    }

    var doDragVertical = function(e) {
        if(e.target.parentElement==null)
            return;

        e.target.parentElement.style.height = (startHeight + e.clientY - startY) + 'px';
    }

    var stopDrag = function(e) {
        document.documentElement.removeEventListener('mousemove', doDragVertical, false);
        document.documentElement.removeEventListener('mousemove', doDragHorizontal, false);
        document.documentElement.removeEventListener('mouseup', stopDrag, false);
        document.documentElement.removeEventListener('mouseout', stopDrag, false);
        onResize(e);
    }

    var onResize = function(e) {
        var parentElId = e.target.parentElement.id;
        var parent = parentElId.split("-"), parentType;
        parentType = parent[0];
        if(parentType == "col") {
            if(typeof(gridCol[parent[1]]) == "undefined")
                return;

            var parentWidth = e.target.parentElement.offsetWidth;
            var cols = gridCol[parent[1]], cellWidth, cell, widthDiff, newWidth;
            for(var i=0; i<cols.length; i++) {
                cell = document.getElementById(cols[i]).parentElement;
                cellWidth = cell.offsetWidth;
                widthDiff = parentWidth - cellWidth;
                newWidth = cellWidth + widthDiff;
                cell.style.setProperty("width", newWidth+"px");
            }
        } else if(parentType == "row") {
            if(typeof(gridRow[parent[1]]) == "undefined")
                return;

            var parentHeight = e.target.parentElement.offsetHeight;
            var rows = gridRow[parent[1]-1], cellHeight, cell, heightDiff, newHeight;
            for(var i=0; i<rows.length; i++) {
                cell = document.getElementById(rows[i]).parentElement;
                cellHeight = cell.offsetHeight;
                heightDiff = parentHeight - cellHeight;
                newHeight = cellHeight + heightDiff;
                cell.style.setProperty("height", newHeight+"px");
            }
        }
    }
}

Resize.init();
