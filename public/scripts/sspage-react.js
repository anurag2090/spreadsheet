import React from 'react';
import ReactDOM from 'react-dom';
import SSPage from '../../src/components/sheet/SSPage';

let component = React.createFactory(SSPage);
ReactDOM.render(component(), document.getElementById('reactContainer'));
